/* Copyright Applied Industrial Logic Limited 2017. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.pageflow;

import static ch.lambdaj.Lambda.DESCENDING;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.sort;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;

import com.ail.core.ExceptionRecord;
import com.ail.core.Type;
import com.ail.core.persistence.hibernate.HibernateSessionBuilder;
import com.ail.insurance.policy.Policy;

/**
 * Page element to display the contents of a quotation's audit history
 */
public class AuditDetails extends PageElement {
	private static final long serialVersionUID = -4810599045554021748L;

	public AuditDetails() {
		super();
	}

	public Map<Number,Policy> getPolicyRevisions() {
	    Map<Number,Policy> policies = new HashMap<>();

        long policyId = PageFlowContext.getPolicySystemId();
        AuditReader auditReader = AuditReaderFactory.get(HibernateSessionBuilder.getSessionFactory().getCurrentSession());
	    List<Number> revisions = auditReader.getRevisions(Policy.class, policyId);

	    for(Number rev: revisions) {
	        policies.put(rev, auditReader.find(Policy.class, policyId, rev));
	    }

	    return policies;
	}

	public List<ExceptionRecord> orderLines(List<ExceptionRecord> lines) {
        return sort(lines, on(ExceptionRecord.class).getDate(), DESCENDING);
    }

    @Override
    public Type renderResponse(Type model) throws IllegalStateException, IOException {
        return executeTemplateCommand("AuditDetails", model);
    }
}
