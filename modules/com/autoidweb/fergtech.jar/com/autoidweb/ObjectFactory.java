
package com.autoidweb.webservices;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.autoidweb.webservices package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.autoidweb.webservices
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ProcessAIWrequest }
     * 
     */
    public ProcessAIWrequest createProcessAIWrequest() {
        return new ProcessAIWrequest();
    }

    /**
     * Create an instance of {@link ArrayOfBase64Binary }
     * 
     */
    public ArrayOfBase64Binary createArrayOfBase64Binary() {
        return new ArrayOfBase64Binary();
    }

    /**
     * Create an instance of {@link ProcessAIWrequestResponse }
     * 
     */
    public ProcessAIWrequestResponse createProcessAIWrequestResponse() {
        return new ProcessAIWrequestResponse();
    }

}
